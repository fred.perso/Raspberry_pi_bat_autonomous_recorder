#!/bin/bash
##
##    -   '-'   -
##   / `.=) (=.` \
##  /.-.-.\ /.-.-.\
##        'v'
##
## RaspPiBat recorder 2015
## Raspberry Pi Model A+/B+ with Cirrus Logic 192kHz SoundBoard // Version 20150727
## Works with old kernel version
## Originally inspired from  "Raspberry Pi®Bat Project" : AK Fledermausschutz Aachen, Düren, Euskirchen (NABU/BUND/LNU)”
## http://www.fledermausschutz.de/forschen/fledermausrufe-aufnehmen/raspberry-pi-bat-project/
## https://www.raspberrypi.org/blog/bat-pi/

## System settings for recording from soundboard
/home/pi/Reset_paths.sh
/home/pi/Record_from_lineIn.sh
export AUDIODRIVER=alsa
export AUDIODEV=hw:1,0

## Recorder ID
export FileNamePrefix="rpibat01"

## Data/Temp/Log directories location
if mount | grep -q /media/usbdisk ; then
		echo "usb disk connected";
		export RpibatDir="/media/usbdisk/rpibat";
		export DataDir="/media/usbdisk/rpibat/data/";
		export TempDir="/media/usbdisk/rpibat/temp/";
		export LogDir="/media/usbdisk/rpibat/log/";
		#mkdir "/media/usbdisk/rpibat/deleted";
		#export DelDir="/media/usbdisk/rpibat/deleted/";
		echo "Data/Temp/Log files will be on USB disk ($RpibatDir)";
	else
		echo "no usb disk";
		export RpibatDir="/rpibat";
		export DataDir="/rpibat/data/";
		export TempDir="/rpibat/temp/";
		export LogDir="/rpibat/log/";
		#mkdir "/rpibat/deleted";
		#export DelDir="/rpibat/deleted/";
		echo "Data/Temp/Log files will be on SDcard ($RpibatDir)";
fi

## Rec global settings
export Buffer="32768"
export SamplingRate="192000"
export BitsDepth="16"

## Rec effects
## Vol settings
export Volume="2"
## High pass filter for filtering low frequencies (sinc settings) 
export Highpassfilter="6k"
## silence settings
export SilenceBeforeSec="0.010"
export TresholdBefore="0.3%"
export SilenceAfterSec="0:00:05"
export TresholdAfter="0.3%"
## Trim settings
export MaxLengthSec="15"

## Timestamps
## Precise timestamp
DateTime() {
date +%Y%m%d_%H%M%S
}

## Simplified timestamps
DateTimeSimp() {
date +%Y%m%d_%H%M
}

## Create a text file to identify the start of the new session
export TimeStampSimp=`DateTimeSimp`
echo "New session" > "$DataDir$FileNamePrefix-N-$TimeStampSimp.txt"

## trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
        echo "** Trapped CTRL-C"
	exit 1
}

while [ 1 ] ; do
export TimeStamp=`DateTime`
AUDIODEV=hw:1,0 /usr/bin/rec -b $BitsDepth --buffer "$Buffer" -L -r $SamplingRate -c 2 --temp "$TempDir" "$DataDir$FileNamePrefix-$TimeStamp.wav" vol $Volume sinc $Highpassfilter silence 1 $SilenceBeforeSec $TresholdBefore 1 $SilenceAfterSec $TresholdAfter trim 0 $MaxLengthSec spectrogram -h -t "$FileNamePrefix" -c "$TimeStamp" -o "$DataDir$FileNamePrefix-$TimeStamp.png"

## re-timestamp recordings and spectrograms
export TimeStampB=`DateTime`
mv "$DataDir$FileNamePrefix-$TimeStamp.wav" "$DataDir$FileNamePrefix-N-$TimeStampB.wav"
mv "$DataDir$FileNamePrefix-$TimeStamp.png" "$DataDir$FileNamePrefix-N-$TimeStampB.png"

## Split audio channels
sndfile-deinterleave "$DataDir$FileNamePrefix-N-$TimeStampB.wav"

## Move original stereo samples to del directory
# mv "$DataDir$FileNamePrefix-N-$TimeStampB.wav" "$DelDir$FileNamePrefix-N-$TimeStampB.wav"

## Delete original stereo samples
rm "$DataDir$FileNamePrefix-N-$TimeStampB.wav"

## Rename splitted mono samples to identify channels (L for left, R for Right)
## Actually, only left channel is used
cd $DataDir ; rename 's/_00.wav/_L.wav/' *.wav ; 
#cd $DataDir ; rename 's/_01.wav/_R.wav/' *.wav ;

## Delete unused channels (when mono)
#cd $DataDir ; rm *_00.wav ; 
#cd $DataDir ; rm *_L.wav ;
cd $DataDir ; rm *_01.wav ; 
#cd $DataDir ; rm *_R.wav ;

#sox "$DataDir$FileNamePrefix-N-$TimeStampB_L.wav" -n spectrogram -h -t "$FileNamePrefix" -c "$TimeStampB" -o "$DataDir$FileNamePrefix-N-$TimeStampB_L.png"
#sox "$DataDir$FileNamePrefix-N-$TimeStampB_R.wav" -n spectrogram -h -t "$FileNamePrefix" -c "$TimeStampB" -o "$DataDir$FileNamePrefix-N-$TimeStampB_R.png"

done