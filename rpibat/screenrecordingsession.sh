#!/bin/bash
##
##   -   '-'   -
##  / `.=) (=.` \
## /.-.-.\ /.-.-.\
##       'v'
##
## RaspPiBat recorder 2015
## Raspberry Pi Model A+/B+ with Cirrus Logic 192kHz SoundBoard // Version 20150727
## Originally inspired from  "Raspberry Pi®Bat Project" : AK Fledermausschutz Aachen, Düren, Euskirchen (NABU/BUND/LNU)”
## http://www.fledermausschutz.de/forschen/fledermausrufe-aufnehmen/raspberry-pi-bat-project/
## https://www.raspberrypi.org/blog/bat-pi/

#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

## Recorder ID
export FileNamePrefix="rpibat01"

## Data/Temp/Log directories location
if mount | grep -q /media/usbdisk ; then
		echo "usb disk connected";
		export RpibatDir="/media/usbdisk/rpibat";
		export DataDir="/media/usbdisk/rpibat/data/";
		export TempDir="/media/usbdisk/rpibat/temp/";
		export LogDir="/media/usbdisk/rpibat/log/";
		echo "Data/Temp/Log files will be on USB disk ($RpibatDir)";
	else
		echo "no usb disk";
		export RpibatDir="/rpibat";
		export DataDir="/rpibat/data/";
		export TempDir="/rpibat/temp/";
		export LogDir="/rpibat/log/";
		echo "Data/Temp/Log files will be on SDcard ($RpibatDir)"; 
fi

## Timestamps
## Precise timestamp
DateTime() {
date +%Y%m%d_%H%M%S
}
export TimeStamp=`DateTime`

## Simplified timestamp
DateTimeSimp() {
date +%Y%m%d_%H%M
}
export TimeStampSimp=`DateTimeSimp`

## screen LogFile name (Default: screenlog.0) set in /rpibat/rpibatscreenrc.rc
export LogFile="$LogDir$FileNamePrefix-$TimeStampSimp.log"
echo -e "lodgir $LogDir\nlogfile $LogFile" > /rpibat/rpibatscreenrc.rc

exec screen -d -m -c /rpibat/rpibatscreenrc.rc -S "$FileNamePrefix" -Dms -L /rpibat/recordings.sh "$FileNamePrefix" 1>> "/$LogDir/recordings_$TimeStampSimp.log" 2>>&1 &