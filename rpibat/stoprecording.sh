#!/bin/bash
##
##   -   '-'   -
##  / `.=) (=.` \
## /.-.-.\ /.-.-.\
##       'v'
##
## RaspPiBat recorder 2015
## Raspberry Pi Model A+/B+ with Cirrus Logic 192kHz SoundBoard // Version 20150727

#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

## Recorder ID
export FileNamePrefix="rpibat01"

## Timestamp
DateTime() {
date +%Y%m%d_%H%M%S
}

export TimeStamp=`DateTime`

## Log directories location
if mount | grep -q /media/usbdisk ; then 
		export LogDir="/media/usbdisk/rpibat/log/";
	else 
		export LogDir="/rpibat/log/";
fi

## Stop recordings
sudo killall screen

## Logs
echo "$TimeStamp : Stop recording" >> "/$LogDir/startstop.log"

## Unmount usbdisk
if sudo fdisk -l | grep -q /dev/sda1 ; then
		sudo /bin/umount /dev/sda1;
fi

## Shutdown
#/sbin/shutdown -h now

exit 0
